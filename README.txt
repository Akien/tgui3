TGUI3 is the latest version of TGUI, a gaming GUI library that has been used
in many Nooskewl products. Each major version has been a complete rewrite,
so TGUI3 shares no API with version 1 or 2. TGUI gets its name from the
author's initials, "Trent Gamblin".

TGUI3 can be used with any framework, but only SDL2 is directly supported.
Since there is only 1 simple function that uses SDL, it's easy to port to
other toolkits. tgui_sdl_convert_event simply converts SDL events to TGUI
events, which you then feed to TGUI in your game loop.

TGUI3's primary purpose is to lay out widgets on screen and make sure events
get distributed correctly. It does no drawing and contains no default
widgets except a base class you create your own widgets with. TGUI_Widget
contains virtual methods for drawing and handling events which you implement
to give your widgets their appearance and behaviour.

Graphical layout resembles CSS in some ways, but is much simpler. By default
widgets align left to right, top to bottom, but you can "float" them to the
right or bottom, break to a new row or center widgets. Each widget can be a
container for other widgets which allows the same layout functionality
inside. Widget sizes can be in pixels or percentages (either percent of
parent or percent of remaining space.) You can set padding on each side of a
widget and position widgets absolutely, as well as several other functions.
